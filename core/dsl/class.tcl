package require http
package require tls
# Тут самописный конфиг
source ./conf.tcl
# Начало запуска парсера
proc Url {} {
    set url [settingsParser 1382237 "sell" 1000]
   
}
# Настраиваем парсер точнее URL для парсера
proc settingsParser {start type {end "false"} } {
    if {$end != "false"} {
        set ends [expr $end + $start]
        set safe_text "start\n"
        while {$ends > $start} {
            set cons [ dict create .start $start .type $type] 
            set config [Conf $cons]
            set urls [ dict create .url $config]
            append safe_text [getPage [dict get $urls .url]]
            append safe_text " ID=$start type=$type \n"
            incr start
        }
        save_file $safe_text
    }
    # return 
}
# Получаем весь HTML
proc getPage {url_to {all_url "false"} } {
        http::register https 443 tls::socket
        set url $url_to
        set token [http::geturl $url -timeout 100000]
        set status [http::status $token]
        if {$status=="ok"} {
            set answer [http::data $token]
            set fins [ParserHTML $answer]
            http::cleanup $token
            http::unregister https
            return $fins
        } else {
            puts "ошибка"
            exit
        }
}

# Начинаем парсер
proc ParserHTML {body} {
    set line_text [regsub -all {<[^>]+>} $body "" newText ]
    set results "Не найдено"
    regexp {(Телефон: [ (8|\+7)[\-]?[\d]{3,6}.[0-9]{3,21})} $newText results 
    puts $results
    # save_file $results
    return $results
    # set url [settingsParser 1382237 "sell"]
    
}

proc save_file {text} {
    if {$text != "" || $text != "false"} {
        set fo [open "phone.txt" "w"] 
            puts -nonewline $fo $text    
        close $fo
    }
}